import {defaultI18n} from './defaultLocalization';
import {Language} from './i18n.interface';
import {NetworkStatus} from 'ctrader-helpers';

export interface IDictionary {
    [key: string]: string;
}

export type I18nStore = {
    locale: Language;
    dictionary: IDictionary;
    networkStatus: NetworkStatus;
};

export const initialLocale = Language.en;

export const i18nInitialState: I18nStore = {
    locale: initialLocale,
    dictionary: {},
    networkStatus: NetworkStatus.None,
};

export interface I18nStoreSegment {
    i18n: I18nStore;
}

export type LocaleKeys = keyof typeof defaultI18n;
