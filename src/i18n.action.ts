import typescriptFsa from 'typescript-fsa';
import {IDictionary} from './i18n.store.interface';
import {Language} from './i18n.interface';

export namespace I18nAction {
    const actionCreator = typescriptFsa('I18n');

    export const set = actionCreator<Language>('SET');
    export const setDone = actionCreator<IDictionary>('SET_DONE');
}
