export enum Language {
    ar = 'ar',
    cs = 'cs',
    de = 'de',
    el = 'el',
    en = 'en',
    es = 'es',
    fr = 'fr',
    hu = 'hu',
    id = 'id',
    it = 'it',
    ja = 'ja',
    ko = 'ko',
    ms = 'ms',
    pl = 'pl',
    pt = 'pt',
    ru = 'ru',
    sk = 'sk',
    sl = 'sl',
    sr = 'sr',
    th = 'th',
    tr = 'tr',
    vi = 'vi',
    zh = 'zh',
}

export interface ILocale {
    value: string;
    title: string;
}

export type ILocales = { [key in Language]: string; };
