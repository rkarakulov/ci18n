import {IDictionary, initialLocale, LocaleKeys, I18nStoreSegment} from './i18n.store.interface';
import {Locales, LocaleNames} from './i18n.constant';
import {ILocale, Language} from './i18n.interface';
import {translationSelectorFactory} from './_selectors/translation.selector';
import {getReduxService, IUrlParamService, toValueTitleArray} from 'ctrader-helpers';
import {IBrokerStoreSegment} from 'ctrader-broker';

export function getAvailableLocales(): ILocale[] {
    return toValueTitleArray(LocaleNames);
}

export function getLocaleByValue(value: string): ILocale {
    return getAvailableLocales()
        .find(locale => locale.value === value);
}

const translationSelector = translationSelectorFactory();

export function L(key: LocaleKeys, ...params: (number | string)[]): string {
    const reduxService = getReduxService<I18nStoreSegment & IBrokerStoreSegment>();

    return reduxService.getSelectedState(translationSelector, {
        key,
        params,
    });
}

export function getLocaleFromParam(urlParamService: IUrlParamService): Language {
    let locale = urlParamService.pop('lang');
    if (!locale || typeof locale !== 'string') {
        return null;
    }

    locale = locale.toLowerCase();

    const existingLocales = Object.keys(Locales);
    if (existingLocales.indexOf(locale) !== -1) {
        return locale;
    }

    return null;
}

// tslint:disable-next-line:cyclomatic-complexity
export const getDictionary = async (locale: Language): Promise<IDictionary> => {
    let dictionary;

    if (locale === Language.ar) {
        // @ts-ignore
        dictionary = await import('../localization/ar_SA.json');
    }
    else if (locale === Language.cs) {
        // @ts-ignore
        dictionary = await import('../localization/cs_CZ.json');
    }
    else if (locale === Language.de) {
        // @ts-ignore
        dictionary = await import('../localization/de_DE.json');
    }
    else if (locale === Language.el) {
        // @ts-ignore
        dictionary = await import('../localization/el_GR.json');
    }
    else if (locale === initialLocale) {
        // @ts-ignore
        dictionary = await import('../localization/en_US.json');
    }
    else if (locale === Language.es) {
        // @ts-ignore
        dictionary = await import('../localization/es_ES.json');
    }
    else if (locale === Language.fr) {
        // @ts-ignore
        dictionary = await import('../localization/fr_FR.json');
    }
    else if (locale === Language.hu) {
        // @ts-ignore
        dictionary = await import('../localization/hu_HU.json');
    }
    else if (locale === Language.id) {
        // @ts-ignore
        dictionary = await import('../localization/id_ID.json');
    }
    else if (locale === Language.it) {
        // @ts-ignore
        dictionary = await import('../localization/it_IT.json');
    }
    else if (locale === Language.ja) {
        // @ts-ignore
        dictionary = await import('../localization/ja_JP.json');
    }
    else if (locale === Language.ko) {
        // @ts-ignore
        dictionary = await import('../localization/ko_KR.json');
    }
    else if (locale === Language.ms) {
        // @ts-ignore
        dictionary = await import('../localization/ms_MS.json');
    }
    else if (locale === Language.pl) {
        // @ts-ignore
        dictionary = await import('../localization/pl_PL.json');
    }
    else if (locale === Language.pt) {
        // @ts-ignore
        dictionary = await import('../localization/pt_PT.json');
    }
    else if (locale === Language.ru) {
        // @ts-ignore
        dictionary = await import('../localization/ru_RU.json');
    }
    else if (locale === Language.sk) {
        // @ts-ignore
        dictionary = await import('../localization/sk_SK.json');
    }
    else if (locale === Language.sl) {
        // @ts-ignore
        dictionary = await import('../localization/sl_SI.json');
    }
    else if (locale === Language.sr) {
        // @ts-ignore
        dictionary = await import('../localization/sr_RS.json');
    }
    else if (locale === Language.th) {
        // @ts-ignore
        dictionary = await import('../localization/th_TH.json');
    }
    else if (locale === Language.tr) {
        // @ts-ignore
        dictionary = await import('../localization/tr_TR.json');
    }
    else if (locale === Language.vi) {
        // @ts-ignore
        dictionary = await import('../localization/vi_VN.json');
    }
    else if (locale === Language.zh) {
        // @ts-ignore
        dictionary = await import('../localization/zh_CN.json');
    }

    return dictionary;
};
