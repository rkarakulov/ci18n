import {createSelector} from 'reselect';
import {I18nStoreSegment, IDictionary, initialLocale} from '../i18n.store.interface';
import {i18nStoreSelector} from '../i18n.store.selector';
import {Language} from '../i18n.interface';
import {createPathSelector, sprintf} from 'ctrader-helpers';
import {IBrokerStoreSegment, brokerDataSelector} from 'ctrader-broker';

export const translationSelectorFactory = () => createSelector<I18nStoreSegment & IBrokerStoreSegment,
    { key: string; params: (number | string)[] },
    string,
    (number | string)[],
    Language,
    IDictionary,
    boolean,
    string>(
    [
        (state, props) => props.key,
        (state, props) => props.params,
        createPathSelector(i18nStoreSelector, 'locale'),
        createPathSelector(i18nStoreSelector, 'dictionary'),
        createPathSelector(brokerDataSelector, 'crowdinEnable'),
    ],
    (key, params, locale, dictionary, crowdinEnable) => {
        if (locale === initialLocale && !crowdinEnable) {
            return params.length
                ? sprintf(key, ...params)
                : key;
        }

        const translatedString = dictionary[key] !== undefined
            ? dictionary[key]
            : key;

        return params.length
            ? sprintf(translatedString, ...params)
            : translatedString;
    },
);
