import {translationSelectorFactory} from '../translation.selector';
import {initialLocale} from '../../i18n.store.interface';
import {Language} from '../../i18n.interface';

describe('translation.selector', () => {
    test('should return key if locale is initial and its no crowdin', () => {
        const actual = translationSelectorFactory()
            .resultFunc('test', [], initialLocale, {}, false);

        const expected = 'test';

        expect(actual)
            .toBe(expected);
    });

    test('should return key printed by params if locale is initial and its no crowdin', () => {
        const actual = translationSelectorFactory()
            .resultFunc('test {0}', ['param1'], initialLocale, {}, false);

        const expected = 'test param1';

        expect(actual)
            .toBe(expected);
    });

    test('should return key if its doesnt found in dictionary', () => {
        const actual = translationSelectorFactory()
            .resultFunc('test', [], Language.ru, {}, false);

        const expected = 'test';

        expect(actual)
            .toBe(expected);
    });

    test('should return key printed by params if its doesnt found in dictionary', () => {
        const actual = translationSelectorFactory()
            .resultFunc('test {0}', ['param1'], Language.ru, {}, false);

        const expected = 'test param1';

        expect(actual)
            .toBe(expected);
    });

    test('should return key changed to dictionary', () => {
        const actual = translationSelectorFactory()
            .resultFunc('test', [], Language.ru, {
                'test': 'meow',
            }, false);

        const expected = 'meow';

        expect(actual)
            .toBe(expected);
    });

    test('should return key printed by params changed to dictionary', () => {
        const actual = translationSelectorFactory()
            .resultFunc('test {0}', ['param1'], Language.ru, {
                'test {0}': 'meow {0}',
            }, false);

        const expected = 'meow param1';

        expect(actual)
            .toBe(expected);
    });
});
