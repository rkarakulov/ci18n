import {I18nStoreSegment, I18nStore} from './i18n.store.interface';

export const i18nStoreSelector = (store: I18nStoreSegment): I18nStore => store.i18n;
