import {I18nAction} from './i18n.action';
import {i18nInitialState} from './i18n.store.interface';
import {reducerWithInitialState} from 'typescript-fsa-reducers';
import {NetworkStatus} from 'ctrader-helpers';

export const i18nReducer = reducerWithInitialState(i18nInitialState)
    .case(I18nAction.set, (state, locale) => {
        return {
            ...state,
            locale,
            networkStatus: NetworkStatus.Started,
        };
    })
    .case(I18nAction.setDone, (state, dictionary) => {
        return {
            ...state,
            dictionary,
            networkStatus: NetworkStatus.Done,
        };
    });
