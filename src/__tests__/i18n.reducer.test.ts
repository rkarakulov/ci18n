import {i18nReducer} from '../i18n.reducer';
import {I18nAction} from '../i18n.action';
import {NetworkStatus} from 'ctrader-helpers';
import {Language} from '../i18n.interface';
import {I18nStore} from '../i18n.store.interface';

describe('i18n.reducer', () => {
    test('should set locale with network status', () => {
        const initialState = {
            locale: Language.ar,
        } as I18nStore;

        const actual = i18nReducer(initialState, I18nAction.set(Language.cs));
        const expected = {
            locale: Language.cs,
            networkStatus: NetworkStatus.Started,
        };

        expect(actual)
            .toEqual(expected);
    });

    test('should set dictionary with network status', () => {
        const initialState = {
            locale: Language.ar,
        } as I18nStore;

        const dictionary = {
            'test': 'test',
        };

        const actual = i18nReducer(initialState, I18nAction.setDone(dictionary));
        const expected = {
            locale: Language.ar,
            dictionary,
            networkStatus: NetworkStatus.Done,
        } as I18nStore;

        expect(actual)
            .toEqual(expected);
    });
});
