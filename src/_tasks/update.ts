import * as fs from 'fs';
import * as gutil from 'gulp-util';
import * as request from 'request';
import {defaultI18n} from '../defaultLocalization';
import {API_KEY, API_URI} from './config';
import {i18nExclusions} from '../i18n.constant';

export function updateFile(next) {
    fs.mkdir(__dirname + '/tmp', error => {
        if (!error || (error && error.code === 'EEXIST')) {

            i18nExclusions.forEach(exclusion => delete defaultI18n[exclusion]);

            fs.writeFile(__dirname + '/tmp/default.json', JSON.stringify(defaultI18n), fileError => {
                if (fileError) {
                    gutil.log('Error create JSON file');
                    return;
                }

                request.post(API_URI + '/update-file', {
                    qs: {
                        key: API_KEY,
                    },
                    formData: {
                        'files[default.json]': fs.createReadStream(__dirname + '/tmp/default.json'),
                    },
                }, () => {
                    fs.unlink(__dirname + '/tmp/default.json', () => {
                        fs.unlink(__dirname + '/tmp', () => {
                            next();
                        });
                    });
                });
            });
        }
        else {
            gutil.log('Error create tmp folder');
        }
    });
}
