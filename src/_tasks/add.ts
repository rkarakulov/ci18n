import * as fs from 'fs';
import * as gutil from 'gulp-util';
import * as request from 'request';
import {defaultI18n} from '../defaultLocalization';
import {API_KEY, API_URI} from './config';

export function addFile(next) {
    fs.mkdir(__dirname + '/tmp', error => {
        if (!error || (error && error.code === 'EEXIST')) {
            fs.writeFile(__dirname + '/tmp/default.json', JSON.stringify(defaultI18n), fileError => {
                if (fileError) {
                    gutil.log('Error create JSON file');
                }

                request.post(API_URI + '/add-file', {
                    qs: {
                        key: API_KEY,
                    },
                    formData: {
                        'files[default.json]': fs.createReadStream(__dirname + '/tmp/default.json'),
                    },
                }, () => {
                    fs.unlink(__dirname + '/tmp/default.json', () => {
                        fs.unlink(__dirname + '/tmp', () => {
                            next();
                        });
                    });
                });
            });
        }
        else {
            gutil.log('Error create tmp folder');
        }
    });
}
