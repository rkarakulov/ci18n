import * as request from 'request';
import {API_KEY, API_URI} from './config';
import {downloadLocale} from './helper';
import {Language} from '../i18n.constant';

export function syncKeys(next) {
    request.get(API_URI + '/export', {
        qs: {
            key: API_KEY,
        },
    }, () => {
        downloadLocale(Language.en, next);
    });
}
