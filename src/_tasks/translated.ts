import * as fs from 'fs';
import * as gutil from 'gulp-util';
import * as Mustache from 'mustache';
import * as path from 'path';

const untranslatedDir = __dirname + '/../untranslated/';

export function makeTranslated(next) {
    fs.readdir(untranslatedDir, (err, files) => {
        make(0, files.filter(file => file !== '.gitignore'), next);
    });
}

function make(index: number, files: string[], next?: () => void) {
    if (index < files.length) {
        const file = files[index];
        const fileName = file.replace('.json', '');

        gutil.log('Start make of ' + fileName);

        const language = require(__dirname + '/../localization/' + fileName + '.ts');

        const langKeys = Object.keys(language);
        const currentTranslation = language[langKeys[0]];
        const currentKeys = Object.keys(currentTranslation);

        const translation = require(untranslatedDir + fileName + '.json');

        const tab = '    ';

        let res = '{' + '\n';

        for (let i = 0; i < currentKeys.length; i++) {
            const currentKey = currentKeys[i];
            const current = currentTranslation[currentKey];

            const formatKey = JSON.stringify(currentKey);
            const formatCurrent = JSON.stringify(current);

            if (translation[currentKey] === undefined) {
                res += tab + formatKey + ': ' + formatCurrent;
            }
            else {
                const formatStr = JSON.stringify(translation[currentKey]);

                if (formatCurrent !== formatStr) {
                    res += tab + formatKey + ': ' + formatStr;
                }
                else {
                    res += tab + formatKey + ': ' + formatCurrent;
                }
            }

            if (i === currentKeys.length - 1) {
                res += '\n';
            }
            else {
                res += ',' + '\n';
            }
        }

        res += '}';

        const tpl = fs.readFileSync(path.join(__dirname, '../_tpl/locale.mst'), 'utf8');

        const rendered = Mustache.render(tpl, {
            code: fileName.replace('_', ''),
            body: res,
        });

        fs.writeFile(__dirname + '/../localization/' + fileName + '.ts', rendered, fileError => {
            if (fileError) {
                gutil.log('Error create ' + fileName + ' file');
            }

            make(index + 1, files, next);
        });
    }
    else {
        gutil.log('Done make all locales');
        next();
    }
}
