import * as fs from 'fs';
import * as gutil from 'gulp-util';
import {Locales, Language} from '../i18n.constant';

const locales = Object.keys(Locales)
                      .filter(locale => locale !== Language.en);

const untranslatedDir = __dirname + '/../untranslated/';

export function makeUntranslated(next) {
    if (!fs.existsSync(untranslatedDir)) {
        fs.mkdirSync(untranslatedDir);
    }

    download(0, next);
}

function download(index: number, next?: () => void) {
    if (index < locales.length) {
        gutil.log('Start make of ' + locales[index]);
        const traderCode = Locales[locales[index]];
        const language = require(__dirname + '/../localization/' + traderCode + '.ts');

        let res = '{' + '\n';

        const langKeys = Object.keys(language);
        const translation = language[langKeys[0]];
        const keys = Object.keys(translation)
                           .filter(key => key === translation[key]);

        for (let i = 0; i < keys.length; i++) {
            const str = JSON.stringify(translation[keys[i]]);
            res += '\t' + str + ': ' + str;

            if (i === keys.length - 1) {
                res += '\n';
            }
            else {
                res += ',' + '\n';
            }
        }

        res += '}';

        fs.writeFile(untranslatedDir + traderCode + '.json', res, fileError => {
            if (fileError) {
                gutil.log('Error create ' + traderCode + ' file', fileError);
            }

            download(index + 1, next);
        });
    }
    else {
        gutil.log('Done make all locales');
        next();
    }
}
