import * as gutil from 'gulp-util';
import * as request from 'request';
import {API_KEY, API_URI} from './config';
import {downloadLocale} from './helper';
import {Language, Locales} from '../i18n.constant';

const locales = Object
    .keys(Locales)
    .filter(locale => locale !== Language.en);

export function downloadTranslations(next) {
    request.get(API_URI + '/export', {
        qs: {
            key: API_KEY,
        },
    }, () => {
        Promise
            .all(locales.map((locale, index) => (
                new Promise(resolve => {
                    setTimeout(() => {
                        gutil.log('Start download of ' + locale);
                        downloadLocale(locale as Language, () => {
                            gutil.log('Done download of ' + locale);
                            resolve();
                        });
                    }, index * 1000);
                })
            )))
            .then(() => {
                gutil.log('Done download all locales');
                next();
            });
    });
}
