import * as fs from 'fs';
import * as gutil from 'gulp-util';
import * as Mustache from 'mustache';
import * as path from 'path';
import * as request from 'request';
import {API_KEY, API_URI} from './config';
import {Language, CrowdinLocales, Locales} from '../i18n.constant';

// locale format should be like en-US
export function downloadLocale(locale: Language, next?: () => void) {
    const crowdinCode = CrowdinLocales[locale];
    const traderCode = Locales[locale];

    request.get(API_URI + '/export-file', {
        qs: {
            file: 'default.json',
            language: crowdinCode,
            key: API_KEY,
        },
    }, (error, response, body) => {
        if (error) {
            gutil.log('FATAL ERROR: ' + error);
            return;
        }

        if (body.indexOf('Maximum concurrent requests for this endpoint reached.') !== -1) {
            gutil.log('FATAL ERROR ' + crowdinCode + ', Maximum concurrent requests');
            if (next) {
                next();
            }
            return;
        }

        if (body.indexOf('Language was not found') !== -1) {
            gutil.log('FATAL ERROR find language ' + crowdinCode + ', please add it in project');
            if (next) {
                next();
            }
            return;
        }

        if (body.indexOf('Specified language is not a part of project target languages') !== -1) {
            gutil.log('Error find language ' + crowdinCode);
            if (next) {
                next();
            }
            return;
        }

        const tpl = fs.readFileSync(path.join(__dirname, '../_tpl/locale.mst'), 'utf8');

        const rendered = Mustache.render(tpl, {
            code: traderCode.replace('_', ''),
            body: body,
        });

        fs.writeFile(__dirname + '/../localization/' + traderCode + '.ts', rendered, fileError => {
            if (fileError) {
                gutil.log('Error create ' + traderCode + ' file');
            }

            if (next) {
                next();
            }
        });
    });
}
