import * as gulp from 'gulp';
import {addFile} from './src/_tasks/add';
import {downloadTranslations} from './src/_tasks/download';
import {syncKeys} from './src/_tasks/sync';
import {makeTranslated} from './src/_tasks/translated';
import {makeUntranslated} from './src/_tasks/untranslated';
import {updateFile} from './src/_tasks/update';

gulp.task('add', addFile);
gulp.task('update', updateFile);
gulp.task('sync', syncKeys);
gulp.task('download', downloadTranslations);
gulp.task('untranslate', makeUntranslated);
gulp.task('translate', makeTranslated);
