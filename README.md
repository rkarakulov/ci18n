## Crowdin Localization Integration

### Usage

##### 1. Go to i18n package
``
cd packages/ctrader-i18n
``

##### 2. Run script
``
npm run update && npm run sync && npm run download
``

1. npm run update - updating our default.ts file to crowdin project
2. npm run sync - syncing crowdin in-context keys to en_US.ts
3. npm run download - downloading translations

### Simplified usage from project's directory
``
cd packages/ctrader-i18n && npm run update && npm run sync && npm run download && cd ../..
``

### Helper scripts for download untranslated string as json file:

1. npm run untranslate - make json files with untranslated strings to the untranslated folder
2. npm run translate - upload json files to the ts files from untranslated folder
